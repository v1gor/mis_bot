# coding: utf8
import telebot
import constants
import requests
import json
from datetime import datetime

bot = telebot.TeleBot(constants.token)

days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

def send_room_info(message, id):
    url = "http://localhost:3000/api/v1/admin/rooms/" + str(id)
    response = requests.get(url)
    data = response.json()
    room = data['room']
    lessons = data['lessons']
    print(data)
    print(lessons)
    # return (data)

def get_rooms():
    response = requests.get("http://localhost:3000/api/v1/admin/rooms")
    data =  response.json()
    data = data['rooms']
    return (data)

def find_id(message):
    buttons = []
    data = get_rooms()
    for i in range(len(data)):
        buttons.append(data[i]['room_name'])
    if message.text in buttons:
        for i in range(len(data)):
            if data[i]['room_name'] == message.text:
                return (data[i]['id'])
    else:
        print("Not correct name of room")
        return False

def main_menu(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row('Monday', 'Tuesday')
    user_markup.row('Wednesday', 'Thursday', 'Friday')
    user_markup.row('Saturday', 'Sunday')
    bot.send_message(message.from_user.id, 'Салем! This is MIS bot', reply_markup = user_markup )
    print(message.from_user.first_name, message.from_user.id)

@bot.message_handler(commands=['start'])
def handle_start(message):
    main_menu(message)

@bot.message_handler(content_types=['text'])
def handle_text(message):
    if message.text in days:
        show_rooms_as_button(message.from_user.id)
    else:
        try:
            id = find_id(message)
            send_room_info(message, id)
        except Exception as e:
            raise
        # send_info(message.text, message.from_user.id)
    # elif message.text in
# response = requests.get("http://185.146.1.49:3000/api/v1/admin/groups")
def date_formater(date):
    date = date.split('T')[1]
    date = date[:5]
    return (date)

def show_rooms_as_button(user_id):
    markup = telebot.types.ReplyKeyboardMarkup(True, True)
    data = get_rooms()
    # global cabs = []
    for i in range(0, len(data) + 1, 2):
        itembtn1, itembtn2 = '', ''
        try:
            print('doing')
            itembtn1 = telebot.types.KeyboardButton(data[i]['room_name'])
            itembtn2 = telebot.types.KeyboardButton(data[i+1]['room_name'])
            print(itembtn1, itembtn2)
        except Exception as e:
            pass
        markup.row(itembtn1, itembtn2)
    bot.send_message(user_id, "Выберите кабинет", reply_markup = markup)

def send_info(day_name, user_id):
    data = get_info(day_name)
    for i in range(len(data)):
        if (day_name == data[i]['day_of_week']):
            student_q = str(data[i]['student_quantity'])
            room_name = data[i]['room_name']
            start_time = date_formater(data[i]['start_date'])
            end_time = date_formater(data[i]['end_time'])
            teacher_name = data[i]['teacher_name']
            message = 'Kабинет: ' + room_name + '\nВремя начала: ' + start_time + '\nВремя конца: ' + end_time + '\nкол-во студентов: ' + student_q + '\nИмя преода: ' + teacher_name
            bot.send_message(user_id, message)
            print(student_q, room_name, start_time, end_time, teacher_name)

# send_info("Monday", 123)
bot.polling(none_stop=True)




# USERNAME = 'arkhat.izbassov@gmail.com' # put correct usename here
# PASSWORD = 'asdasdasd' # put correct password here
#
# LOGINURL = 'http://185.146.1.49:3000/'
# DATAURL = 'http://185.146.1.49:3000/api/v1/admin/groups'
#
# session = requests.session()
#
# req_headers = {
#     'Content-Type': 'application/x-www-form-urlencoded'
# }
#
# formdata = {
#     'UserName': USERNAME,
#     'Password': PASSWORD,
#     'LoginButton' : 'Sign in'
# }
#
# # Authenticate
# r = session.post(LOGINURL, data=formdata, headers=req_headers, allow_redirects=False)
# print (r.headers)
# print (r.status_code)
# print (r.text)
#
# # Read data
# r2 = session.get(DATAURL)
# print ("___________DATA____________")
# print (r2.headers)
# print (r2.status_code)
# print (r2.text)

# response = requests.get("http://185.146.1.49:3000/api/v1/admin/groups")
# response = requests.get("http://localhost:3000/api/v1/admin/bots")
# data =  response.json()
# print (data)
